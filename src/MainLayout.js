import React, {useState, useContext} from 'react';
import {View, StyleSheet, Alert} from "react-native";

import NavBar from "./components/specifications/NavBar/NavBar";
import MainScreen from "./screens/MainScreen/MainScreen";
import ToDoScreen from "./screens/ToDoScreen/ToDoScreen";
import {ScreenContext} from "./context/screen/screenContext";

import ss from "./specificationsCSS";

const s = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    container: {
        ...ss.container,
        margin: 0,
        flex: 1,
        marginLeft: 'auto',
        marginRight: 'auto',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
});

const MainLayout = () => {
    const {toDoId} = useContext(ScreenContext);

    return (
        <View style={s.wrapper}>
            <NavBar title="ToDo App" />

            <View style={s.container}>
                {toDoId ? <ToDoScreen /> : <MainScreen />}
            </View>
        </View>
    );
};

export default MainLayout;

