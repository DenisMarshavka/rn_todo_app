import React, {useEffect, useContext, useCallback} from 'react';
import {View} from 'react-native';

import AddToDoContainer from "../../components/MainScreen/AddToDoContainer/AddToDoConainer";
import ToDoList from "../../components/MainScreen/ToDoList/ToDoList";
import {AppLoader} from "../../components/specifications/UI/AppLoader/AppLoader";
import {AppText} from "../../components/specifications/UI/AppText/AppText";
import {AppButton} from "../../components/specifications/UI/AppButton/AppButton";

import {ToDoContext} from "../../context/toDo/toDoContext";
import {ScreenContext} from "../../context/screen/screenContext";

import s from './MainScreen.moduleCSS';

const MainScreen = () => {
    const { toDos, loading, error, fetchToDos, addToDo, removeToDo } = useContext(ToDoContext);
    const {changeScreen} = useContext(ScreenContext);

    const loadToDos = useCallback(async () => await fetchToDos(), [fetchToDos]);

    useEffect(() => {
        loadToDos();
    }, []);

    if (loading) return <AppLoader />;

    if (error) {
        return (
            <View style={s.center}>
                <AppText style={s.error}>{error}</AppText>

                <AppButton styleWrap={s.errorWrapBtn} color="transparent" colorText={s.errorBtnText} onPress={loadToDos}>Try again</AppButton>
            </View>
        );
    }

    return (
        <View style={s.wrapper}>
            <AddToDoContainer onSubmit={addToDo} />

            <ToDoList toDos={toDos} removeToDo={removeToDo} onOpen={changeScreen} />
        </View>
    );
};

export default MainScreen;
