import {StyleSheet} from 'react-native';
import {THEME} from "../../theme";

export default StyleSheet.create({
    wrapper: {
        width: '100%',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    },

    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    error: {
        fontSize: 18,
        color: THEME.DANDER_COLOR,
    },

    errorWrapBtn: {
        marginTop: 20,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: THEME.DARK_COLOR,
    },

    errorBtnText: {
        color: THEME.DARK_COLOR,
    },
});
