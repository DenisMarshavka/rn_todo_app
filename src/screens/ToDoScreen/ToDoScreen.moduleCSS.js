import {StyleSheet, Dimensions} from 'react-native';

export default StyleSheet.create({
    container: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },

    card: {
      marginBottom: 35
    },

    title: {
        fontSize: 18,
    },

    btnEdit: {
      marginLeft: 5,
    },

    buttons: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    button: {
        // width: '45%',
        width: Dimensions.get('window').width / 3,
    },
});
