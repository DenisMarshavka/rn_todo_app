import React, {useState, useContext} from 'react';
import {Keyboard, View} from 'react-native';
import {AntDesign, MaterialCommunityIcons} from '@expo/vector-icons';

import {AppCard} from "../../components/specifications/UI/AppCard/AppCard";
import DoModalEdit from "../../components/ToDoScreen/DoModalEdit/DoModalEdit";
import {AppTextBold} from "../../components/specifications/UI/AppTextBold/AppTextBold";
import {AppButton} from "../../components/specifications/UI/AppButton/AppButton";
import {ToDoContext} from "../../context/toDo/toDoContext";
import {ScreenContext} from "../../context/screen/screenContext";

import s from './ToDoScreen.moduleCSS';
import {THEME} from "../../theme";

const ToDoScreen = () => {
    const { toDos, removeToDo, updateToDo } = useContext(ToDoContext);
    const { toDoId, changeScreen } = useContext(ScreenContext);
    const [modal, setModal] = useState(false);

    const toDo = toDos.find(t => t.id === toDoId);

    const handleModalCancel = () => setModal(false);

    const handleNameUpdate = async title => {
        Keyboard.dismiss();

        await updateToDo(toDo.id, title);
        setModal(false);
    };

    return (
        <View style={s.container}>
            <DoModalEdit value={toDo.title} visible={modal} onCancel={handleModalCancel} onSave={handleNameUpdate} />

            <AppCard style={s.card}>
                <AppTextBold style={s.title}>{toDo.title}</AppTextBold>

                <AppButton style={s.btnEdit} onPress={() => setModal(true)}>
                    <AntDesign name="edit" size={20}/>
                </AppButton>
            </AppCard>

            <View style={s.buttons}>
                <View style={s.button}>
                    <AppButton color={THEME.LIGHT_COLOR} onPress={() => changeScreen(null)}>
                        <MaterialCommunityIcons name="backburger" size={15}/>
                    </AppButton>
                </View>

                <View style={s.button}>
                    <AppButton title="" color={THEME.DANDER_COLOR} onPress={() => removeToDo(toDo.id)}>
                        <AntDesign name="delete" size={15}/>
                    </AppButton>
                </View>
            </View>
        </View>
    );
};

export default ToDoScreen;
