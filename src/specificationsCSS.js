import {StyleSheet} from 'react-native';

export const FIREBASE_SRC = 'https://rn-todo-app-2c347.firebaseio.com';

export const padding = (a, b, c, d) => {
    return {
        paddingTop: a,
        paddingRight: b ? b : a,
        paddingBottom: c ? c : a,
        paddingLeft: d ? d : (b ? b : a)
    }
};

export default StyleSheet.create({
   container: {
       margin: 0,
       marginLeft: 'auto',
       marginRight: 'auto',
       flexDirection: 'row',
       width: '90%'
   },
});
