import {StyleSheet} from 'react-native';
import {THEME} from "../../../../theme";

export default StyleSheet.create({
    default: {
        padding: 10,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

        borderWidth: 2,
        borderRadius: 10,
        borderColor: THEME.PRIMARY_COLOR,
        backgroundColor: THEME.PRIMARY_COLOR,

        shadowColor: THEME.DARK_COLOR,
        shadowRadius: 4,
        shadowOpacity: .2,
        shadowOffset: {height: 4},
        elevation: 7,//Box Shadow for Android
    }
});
