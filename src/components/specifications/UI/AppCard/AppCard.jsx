import React from 'react';
import {View} from 'react-native';

import s from './AppCard.moduleCSS';

export const AppCard = props => {
    return <View style={ {...s.default, ...props.style} }>{props.children}</View>;
};
