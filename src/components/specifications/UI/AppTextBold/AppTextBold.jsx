import React from 'react';
import {StyleSheet, Text} from 'react-native';

export const AppTextBold = ({ children, style }) => {
    return (
        <Text style={{ ...s.default, ...style }}>{children}</Text>
    );
};

const s = StyleSheet.create({
    default: {
        fontFamily: 'roboto-bold',
    }
});
