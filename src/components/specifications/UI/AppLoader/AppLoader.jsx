import React from 'react'
import {View, StyleSheet, ActivityIndicator} from 'react-native'

import {THEME} from "../../../../theme"

export const AppLoader = () => (<View style={s.center}><ActivityIndicator size="large" color={THEME.DARK_COLOR} /></View>);

const s = StyleSheet.create({
    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
});
