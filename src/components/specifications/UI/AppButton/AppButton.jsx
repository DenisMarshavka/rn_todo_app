import React from 'react';
import {View, StyleSheet, TouchableOpacity, TouchableNativeFeedback, Platform} from 'react-native';
import {AppTextBold} from "../AppTextBold/AppTextBold";

import {THEME} from "../../../../theme";

export const AppButton = ({ children, onPress, color = THEME.MAIN_COLOR, styleWrap, colorText, style }) => {
    const Wrapper = Platform.OS === 'android' ? TouchableNativeFeedback : TouchableOpacity;

    return (
        <Wrapper style={{ ...styleWrap || '' }} onPress={onPress} activeOpacity={.7}>
            <View style={{ ...s.button, ...style, backgroundColor: color }}>
                <AppTextBold style={{ ...s.text, ...colorText }}>{children}</AppTextBold>
            </View>
        </Wrapper>
    );
};

const s = StyleSheet.create({
    button: {
        paddingHorizontal: THEME.PADDING_HORIZONTAL,
        paddingVertical: THEME.PADDING_VERTICAL,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',

        borderRadius: 5,
    },

    text: {
        color: '#fff',
    }
});
