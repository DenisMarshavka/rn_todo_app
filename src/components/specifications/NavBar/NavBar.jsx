import React from 'react';
import {View, Platform} from 'react-native';

import {AppTextBold} from "../UI/AppTextBold/AppTextBold";

import s from './NavBar.moduleCSS';

const NavBar = ({title}) => {
    return (
        <View style={{ ...s.navBarBox, ...Platform.select({ios: s.navBarBoxIOS, android: s.navBarBoxAndroid}) }}>
            <AppTextBold style={s.text}>{title}</AppTextBold>
        </View>
    );
};

export default NavBar;
