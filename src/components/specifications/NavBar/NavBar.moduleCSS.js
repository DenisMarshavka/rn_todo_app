import {StyleSheet} from "react-native";
import {THEME} from "../../../theme";

export default StyleSheet.create({
    navBarBox: {
        width: '100%',
        height: 70,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-end',
        marginBottom: 20,
    },

    navBarBoxAndroid: {
        backgroundColor: THEME.MAIN_COLOR,
    },

    navBarBoxIOS: {
        borderBottomWidth: 1,
        borderBottomColor: THEME.DARK_COLOR,
    },

    text: {
        color: THEME.DARK_COLOR,
        paddingBottom: 10,
        fontSize: 20,
    }
});
