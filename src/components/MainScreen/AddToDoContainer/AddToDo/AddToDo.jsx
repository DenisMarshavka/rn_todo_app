import React from "react";
import { View, TextInput, StyleSheet } from "react-native";
import { Entypo } from "@expo/vector-icons";

import ss from "../../../../specificationsCSS";
import { THEME } from "../../../../theme";

const s = StyleSheet.create({
  wrapper: {
    ...ss.container,
    justifyContent: "space-between",
    width: "100%",
    marginBottom: 35,
  },

  input: {
    flex: 1,
    padding: 10,
    paddingLeft: 0,
    paddingRight: 0,

    width: "70%",
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderBottomColor: THEME.PRIMARY_COLOR,
  },
});

const AddToDo = ({ handlerPress, setValue, currentValue }) => {
  return (
    <View style={s.wrapper}>
      <TextInput
        style={s.input}
        onChangeText={setValue}
        value={currentValue}
        placeholder="Please enter a name your todo"
        autoCorrect={false}
      />

      <Entypo.Button onPress={handlerPress} name="add-to-list">
        Add
      </Entypo.Button>
      {/*<Button title="Add" onPress={handlerPress}/>*/}
    </View>
  );
};

export default AddToDo;
