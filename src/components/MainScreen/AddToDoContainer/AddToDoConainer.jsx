import React, {useState} from 'react';
import {Alert, Keyboard} from 'react-native';

import AddToDo from "./AddToDo/AddToDo";

const AddToDoContainer = ({onSubmit}) => {
    const [value, setValue] = useState('');

    const handlerPress = () => {
        if (value.trim().length <= 3) {
            Alert.alert('Helper', 'The name should have 3 lengths of symbols minimum');
        } else {
            onSubmit(value);
            setValue('');

            Keyboard.dismiss();
        }
    };

    return (
        <AddToDo setValue={setValue} handlerPress={handlerPress} currentValue={value}/>
    );
};

export default AddToDoContainer;
