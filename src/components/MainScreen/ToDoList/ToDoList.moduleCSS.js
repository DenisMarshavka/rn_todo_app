import { StyleSheet } from "react-native";

export default StyleSheet.create({
  toDoListsWrap: {
    width: "100%",
    justifyContent: "flex-start",
    alignItems: "center",
    flexDirection: "column",
    paddingBottom: 50,
  },

  imgWrap: {
    width: "100%",
    height: "100%",
    maxHeight: "65%",

    alignItems: "center",
    justifyContent: "center",
  },

  img: {
    flex: 1,
    width: "100%",
    resizeMode: "center",
  },

  toDoLists: {
    width: "100%",
    height: "100%",
    maxHeight: "90%",
  },
});
