import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";

import { padding } from "../../../../specificationsCSS";
import { THEME } from "../../../../theme";
import { AppTextBold } from "../../../specifications/UI/AppTextBold/AppTextBold";

const s = StyleSheet.create({
  toDo: {
    flex: 1,
    width: "100%",
    marginBottom: 15,
    justifyContent: "flex-start",
    alignItems: "center",
    flexDirection: "row",

    borderWidth: 1,
    borderColor: THEME.PRIMARY_COLOR,
    borderRadius: 5,

    backgroundColor: THEME.PRIMARY_COLOR,
  },
});

const ToDo = ({ id, title, removeToDo, onOpen }) => {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={onOpen.bind(null, id)}
      onLongPress={removeToDo.bind(null, id)}
    >
      <View style={{ ...s.toDo, ...padding(10, 15) }}>
        <AppTextBold>{title}</AppTextBold>
      </View>
    </TouchableOpacity>
  );
};

export default ToDo;
