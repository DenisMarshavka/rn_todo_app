import React, {useState, useEffect} from 'react';
import {View, FlatList, Image, Dimensions} from 'react-native';

import ToDo from "./ToDo/ToDo";

import s from './ToDoList.moduleCSS';
import {THEME} from "../../../theme";

const ToDoList = ({toDos, removeToDo, onOpen}) => {
    const [deviceWidth, setDeviceWidth] = useState(Dimensions.get('window').width - (THEME.PADDING_HORIZONTAL * 2));

    useEffect(() => {
        const update = () => {
            const width = Dimensions.get('window').width - (THEME.PADDING_HORIZONTAL * 2);

            setDeviceWidth(width);
        };

        Dimensions.addEventListener('change', update);

        return () => Dimensions.removeEventListener('change', update);
    }, []);

    let content = (
        <View style={{ width: deviceWidth, overflow: 'hidden' }}>
            <FlatList
                style={s.toDoLists} data={toDos}
                keyExtractor={item => item.id} renderItem={({item}) => <ToDo title={item.title} id={item.id} removeToDo={removeToDo} onOpen={onOpen} />}
            />
        </View>
    );

    if (!toDos.length) {
        content = (
            <View style={s.imgWrap}>
                <Image style={s.img} source={require('../../../../assets/specifications/icons/no-items.png')}/>
            </View>
        );
    }

    return (
        <View style={s.toDoListsWrap}>
            {content}
        </View>
    );
};

export default ToDoList;
