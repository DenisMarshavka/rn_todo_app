import React, {useState} from 'react'
import {View, TextInput, Modal, Alert} from 'react-native'

import s from './DoModalEdit.moduleCSS'
import {THEME} from "../../../theme";
import {AppButton} from "../../specifications/UI/AppButton/AppButton";

const DoModalEdit = ({ value, visible, onCancel, onSave }) => {
    const [newName, setNewName] = useState(value);

    const handleSaveName = () => {
        if (newName.trim().length <= 3) {
            Alert.alert('Error!', 'The name should have 3 lengths of symbols minimum');
        } else onSave(newName);
    };

    const handleCancel = () => {
        setNewName(value);
        onCancel();
    };

    return (
        <Modal visible={visible} animationType="slide" transparent={false}>
            <View style={s.wrap}>
                <TextInput
                    value={newName} style={s.input}
                    placeholder="Enter the new name of the case" autoCorrect={false}
                    maxLength={45} onChangeText={setNewName}
                />

                <View style={s.buttons}>
                    <AppButton styleWrap={s.buttonWrap} color={THEME.DANDER_COLOR} onPress={handleCancel}>Cancel</AppButton>

                    <AppButton styleWrap={s.buttonWrap} onPress={handleSaveName}>Save</AppButton>
                </View>
            </View>
        </Modal>
    );
};

export default DoModalEdit;
