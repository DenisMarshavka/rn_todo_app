import {StyleSheet} from 'react-native';
import {THEME} from "../../../theme";

export default StyleSheet.create({
    wrap: {
        width: '90%',
        margin: 0,
        marginRight: 'auto',
        marginLeft: 'auto',

        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },

    input: {
        padding: 10,
        borderBottomWidth: 2,
        borderBottomColor: THEME.PRIMARY_COLOR,
        paddingLeft: 0,
        width: '90%',
        marginBottom: 20
    },

    buttons: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'center',
    },

    buttonWrap: {
        width: '100%',
        maxWidth: '40%',
    },
});
