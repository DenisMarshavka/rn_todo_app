import {
    ADD_TODO,
    CLEAR_ERROR,
    FETCH_TODOS,
    HIDE_LOADER,
    REMOVE_TODO,
    SHOW_ERROR,
    SHOW_LOADER,
    UPDATE_TODO
} from "../../types";

const handlers = {
    [ADD_TODO]: (state, { id, title }) => (
        {
            ...state,
            toDos: [
                { id, title },
                ...state.toDos
            ]
        }
    ),

    [REMOVE_TODO]: (state, {id}) => (
        {
            ...state,
            toDos: state.toDos.filter(toDo => toDo.id !== id)
        }
    ),

    [UPDATE_TODO]: (state, {id, title}) => (
        {
            ...state,
            toDos: state.toDos.map(toDo => {
                if (id === toDo.id) toDo.title = title;

                return toDo;
            })
        }
    ),

    [SHOW_LOADER]: state => ({...state, loading: true}),
    [HIDE_LOADER]: state => ({...state, loading: false}),

    [SHOW_ERROR]: (state, {error}) => ({ ...state, error }),
    [CLEAR_ERROR]: state => ({ ...state, error: null }),

    [FETCH_TODOS]: (state, {toDos}) => ({ ...state, toDos }),

    DEFAULT: state => state
};

export const toDoReducer = (state, action) => {
    const handler = handlers[action.type] || handlers.DEFAULT;

    return handler(state, action);
};
