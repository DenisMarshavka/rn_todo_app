import React, {useReducer, useContext} from 'react';
import {Alert} from 'react-native';

import {ToDoContext} from './toDoContext';
import {toDoReducer} from "./toDoReducer";
import {
    ADD_TODO,
    CLEAR_ERROR,
    FETCH_TODOS,
    HIDE_LOADER,
    REMOVE_TODO,
    SHOW_ERROR,
    SHOW_LOADER,
    UPDATE_TODO
} from "../../types";
import {ScreenContext} from "../screen/screenContext";
import {FIREBASE_SRC} from "../../specificationsCSS";

const initialState = {
    toDos: [],
    loading: false,
    error: null
};

export const ToDoState = ({ children }) => {
    const [state, dispatch] = useReducer(toDoReducer, initialState);
    const {changeScreen} = useContext(ScreenContext);

    const showError = error => dispatch({ type: SHOW_ERROR, error });
    const clearError = () => dispatch({type: CLEAR_ERROR});

    const showLoader = () => dispatch({type: SHOW_LOADER});
    const hideLoader = () => dispatch({type: HIDE_LOADER});

    const addToDo = async title => {
        clearError();

        try {
            const response = await fetch(`${FIREBASE_SRC}/toDos.json`,{
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({title})
            });

            const data = await response.json();

            dispatch({ type: ADD_TODO, title, id: data.name });
        } catch (e) {
            showError('Something went wrong... :( Code: 1');

            console.log(e);
        }
    };

    const fetchToDos = async () => {
        showLoader();

        clearError();

        try {
            const response = await fetch(`${FIREBASE_SRC}/toDos.json`, {
                method: 'GET', //Is Default
                headers: {'Content-Type': 'application/json'},
            });

            const data = await response.json();
            const toDos = ( data ? Object.keys(data).map(key => ({ ...data[key], id: key })) : [] );

            dispatch({ type: FETCH_TODOS, toDos });
        } catch (e) {
            showError('Something went wrong... :( Code: 2');

            console.log(e);
        } finally {
            hideLoader();
        }
    };

    const removeToDo = id => {
        const toDo = state.toDos.find(t => t.id === id);

        Alert.alert(
            'Remove case',
            `Are you sure about removing this case "${toDo.title}" ?`,

            [
                {
                    text: 'Not',
                    style: 'cancel',
                },

                {
                    text: 'Yes',
                    style: 'destructive',
                    onPress: async () => {
                        await fetch(`${FIREBASE_SRC}/toDos/${id}.json`, {
                            method: 'DELETE',
                            headers: {'Content-Type': 'application/json'}
                        });

                        changeScreen(null);
                        dispatch({ type: REMOVE_TODO, id });
                    },
                },
            ],

            {cancelable: true},
        );
    };

    const updateToDo = async (id, title) => {
        clearError();

        try {
            await fetch(`${FIREBASE_SRC}/toDos/${id}.json`, {
                method: 'PATCH',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({title})
            });

            dispatch({ type: UPDATE_TODO, id, title });
        } catch (e) {
            showError('Something went wrong... :( Code: 3');

            console.log(e);
        }
    };

    return <ToDoContext.Provider value={{ toDos: state.toDos, loading: state.loading, error: state.error, fetchToDos, addToDo, removeToDo, updateToDo }}>{children}</ToDoContext.Provider>;
};
