import React, {useState} from 'react';
import {View, Alert} from 'react-native';
import * as Font from 'expo-font';
import {AppLoading} from 'expo';

import MainLayout from "./src/MainLayout";
import {ToDoState} from "./src/context/toDo/ToDoState";
import {ScreenState} from "./src/context/screen/ScreenState";

const loadApplication = async () => {
    await Font.loadAsync({
        'roboto-regular': require('./assets/fonts/Roboto/Roboto-Regular.ttf'),
        'roboto-bold': require('./assets/fonts/Roboto/Roboto-Bold.ttf'),
    });
};

export default function App() {
    const [isReady, setIsReady] = useState(false);

    if (!isReady) {
        return (
            <AppLoading
                startAsync={loadApplication} onError={e => console.log('Error building!', e)}
                onFinish={() => setIsReady(true)}
            />
        );
    }

    return (
        <ScreenState>
            <ToDoState>
                <MainLayout />
            </ToDoState>
        </ScreenState>
    );
}
